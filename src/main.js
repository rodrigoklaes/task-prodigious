import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import VueCarousel from 'vue-carousel'
import Geocoder from "@pderas/vue2-geocoder";



Vue.config.productionTip = false

Vue.use(Geocoder, {
  googleMapsApiKey: 'AIzaSyBVV02QMbdjEbFNaxbV-avjKp874rS0k28'
});
Vue.use(VueMaterial)
Vue.use(VueCarousel)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
